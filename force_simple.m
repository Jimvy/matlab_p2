function force = force_simple(x)
    R=6e-03;
    a=9.5e-03;
    I=0.5;
    B=1;
    
    if (abs(x)>R+a) %pas d'intersection
        force=0;
        return;
    end
    if (abs(x) < (a-R)) %aimant sur bobine
        x=a-R;
    end
    phi=acos((x*x+R*R-a*a)/(2*R*abs(x)));
    force=2*I*R*B*sin(phi);
    if (x>0)
        force=-force;
    end
end
