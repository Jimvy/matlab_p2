function tension = sense(affichage, aimant, position_bobine, ...
    orientation_bobine, dipolar, vitesse_aimant)
% Cette fonction calcule la tension aux bornes de la bobine, induite par le
% passage de l'aimant au-dessus d'elle.

%TODO remettre le check de l'affichage (meme s'il sert a rien)
if (sum(affichage)>0)
    disp('Desole, je n''aime pas afficher des trucs');
end
    %{
    Basiquement, ca fonctionne ainsi :
    on sait que epsilon = d/dt x integrale de B . dS
    Or, ici, on voit que ce qui change, ce n'est pas le flux intercepte
    par la surface, mais celle interceptee par des petits elements de
    surface sur les segments de la boucle qui bouge. On peut donc se
    contenter d'etudier ces changements, ce qui conduit a une relation
    lineaire avec la vitesse, et avec une integrale de B * dl, avec un
    produit scalaire un peu bizarre (vecteur de norme dl et orthogonal a la
    surface de la boucle. Donc, produit scalaire avec la verticale, donc
    composante en z verticale.
    %}
    
    % On peut montrer (cf rapport et feuilles) que epsilon = v dot F/I, ou
    % F est la force exercee sur l'aimant par la bobine (via la fonction
    % drive) et I est le courant la traversant. On peut donc calculer :
    force_par_courant = drive([0 0 0 0 0], aimant, position_bobine, orientation_bobine, ...
        dipolar, 1); % courant unitaire
    tension = dot(vitesse_aimant, force_par_courant);
    
    %% Affichage des resultats
    disp('Tension induite aux bornes de la bobine :');
    disp(tension);
    
end

