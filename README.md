Repo du code MATLAB pour le cours de Projet Q2 à l'Ecole Polytechnique de Louvain.
==================================================================================

Le projet de cette année était la conception d'un pendule autonome mû par l'énergie solaire (oui, ce n'est pas très excitant).
Le dispositif était constitué d'un aimant attaché à une corde et d'un circuit électronique alimenté par l'énergie solaire, dont le principal composant était une bobine.
La bobine détectait le passage de l'aimant grâce à l'induction électromagnétique, et donnait alors une impulsion magnétique pour maintenir l'aimant en mouvement.

Dans le cadre du projet, nous avons dû modéliser et simuler les interactions entre la bobine et l'aimant à laide du logiciel MATLAB.
Ces interactions consistaient en deux types :

* la détection de l'aimant par la bobine par induction, fonction dénommée _SENSE_ ;
* l'impulsion magnétique généré par la bobine, et la force alors exercée sur l'aimant, dénommée _DRIVE_.


Les différents fichiers préjents implémentent ces fonctions :

* `SimulationBobineAimant.m` : fichier original donné par les professeurs, que l'on a complété par la suite.
* `drive.m` : fonction implémentant _DRIVE_ : calcul de la force exercée par la bobine sur l'aimant.
* `force_simple.m` : fonction implémentant un modèle simplifié et approché de _DRIVE_.
* `sense.m` : fonction implémentant _SENSE_ : potentiel aux bornes de la bobine lors du passage de l'aimant.
* `main.m` : fonction principale définissant les paramètres de la bobine et appelant les fonctions spécialisées ; lors du rendu finale, la fonction _DRIVE_ y a été rajoutée.
* `affichage_mesures.m` : script utilitaire permettant d'afficher les graphes du modèle simplifié, de la simulation MATLAB et de données expérimentales fournies.