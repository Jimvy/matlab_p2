
function [positions_x, angles_x, prevForces, forces]=main()
% Fonction principale du programme

%% Geometrie du systeme
    longueur = 0.1; % de la corde, ou le rayon du tourne-disque
    ecart = 0.005;
    angles_x=linspace(-pi/4, pi/4, 401)'; % les angles pour lesquels on calcule la force
    positions_x=linspace(-0.1, 0.1, 401)';
    
%% Geometrie de l'aimant

    champ = 1; % [T]
    % dimensions en metres
    % si cylindrique
    rayon = 9.5e-3;
    hauteur = 3e-3;
    volume = pi*rayon^2*hauteur;
    
    
    ech_aimant = [20 20 10]; % discretisation de l'aimant dans les directions x, y et z.
    dipolar_momentum = 1.8e6*volume*champ; % Moment dipolaire total de l'aimant
    
    %tilt=pi/12; % l'angle, 15�, pour les tests
    
    omega=2*pi*2.5;
    
%% Geometrie de la bobine
    rayon_interne_bobine = 2e-3; % rayon de la bobine
    rayon_externe_bobine = 10e-3;
    hauteur_bobine       = 3e-3;
    
    ech_bobine       = 32; % discretisation de la bobine sur une boucle
    nbCouches_bobine = 4;
    nbCercles_bobine = 30; %todo modifier ces valeurs pour le programme final
    
    courant = 0.5; % A
        
%% Discretisation de la bobine
    [position_bobine, orientation_bobine] = discretize_coil(...
        rayon_interne_bobine, rayon_externe_bobine, hauteur_bobine, ...
        ech_bobine, nbCouches_bobine, nbCercles_bobine);
    % position bobine : ech_bobine x 3 ; une ligne : x, y, z d'un element
    % orientation_bobine : ech_bobine x 3 ; une ligne : vecteur
    % d'orientation d'un element dl de la bobine
    
    
%% Discretisation de l'aimant
% Ce bloc de code calcule la position de chacun des dipoles composant
% l'aimant.
    % Calcul des positions des dipoles relatives au centre
    aimant = discretize_magnet([rayon, hauteur], ech_aimant);
    % aimant : ech_aimant x 3 ; chaque ligne est la position d'un morceau
    % d'aimant, avec le dipolar_momentum divise par les echantillons
    
    
    % Code permettant de juste calculer la force en un seul point
    
    %drive([0 0 1 1 1], bsxfun(@plus, tilt_magnet(aimant, tilt), [0.001 0 0.010]), tilt, ...
    %    position_bobine, orientation_bobine, dipolar_momentum, courant);
    %sense(affichage, aimant, position_bobine, orientation_bobine,...
     %   dipolar_momentum, [0.10 0 0]);
    
%% Calcul de la force pour chaque distance
    % on va donc definir chaque distance, puis calculer la force partout
    % SPDG, on considere que l'aimant ne se deplace que le long de l'axe x, et
    % qu'il est a 1cm de la bobine (par surete). Voir distant_force
    
    
    forces=distant_force(aimant, position_bobine, orientation_bobine, ...
        dipolar_momentum, courant, angles_x, longueur, ecart, omega);
    prevForces=prev_distant_force(aimant,position_bobine,orientation_bobine,...
        dipolar_momentum, courant, ecart, positions_x);
end

function forces_x = prev_distant_force(aimant, position_bobine, ...
    orientation_bobine, dipolar_momentum, courant, ecart, positions_x)
    nule=[0 0 0 0 0]; % n'affiche rien
    
    positions=[positions_x, zeros(size(positions_x)), 0.01*ones(size(positions_x))];
    forces=zeros(size(positions));
    for it=1:size(positions, 1)
        forces(it, :)= drive(nule, bsxfun(@plus, aimant, positions(it, :)), ...
            0, position_bobine, orientation_bobine, dipolar_momentum, courant);
    end
    % reste a calculer la norme de la force utile
    forces_x=forces(:, 1);
    figure;
    plot(positions_x, forces_x, 'r'); % Dans notre cas, la composante
    % en y est nulle et z n'influence pas.
    title('Force exerc�e sur l''aimant, ancienne trajectoire');
end

function forces_x = distant_force(aimant,position_bobine,orientation_bobine,...
    dipolar_momentum, courant, positions_x, longueur, ecart, omega)
    % Cette fonction calcule, pour differentes distances, la force exercee
    % sur l'aimant par la bobine, en supposant une vitesse constante.
    % SPDG, l'aimant se deplace sur l'axe x (points definis dans
    % positions_x), avec y=0 et z=1cm
    % Depuis, la fonction a change : positions_x contient les angles a
    % analyser, et on calcule de la les positions et les forces.
    
    nule=[0 0 0 0 0]; % n'affiche rien
    
    positions=[longueur.*sin(positions_x), zeros(size(positions_x)), ((ecart+longueur)*ones(size(positions_x))-longueur*cos(positions_x))];
        %ecart*ones(size(positions_x))]
    %pousitions_x = positions(:, 1);    
    
    size(positions)
    forces = zeros(size(positions));
    tensions = zeros(size(positions));
    % une optimisation possible serait de rendre drive vectoriel.
    %drive(nule, bsxfun(@plus, aimant, positions(1, :)), ...
    %        -positions_x(1), position_bobine, orientation_bobine, dipolar_momentum, courant)
    afficherQuellePosition=round(linspace(1, size(positions_x, 1), 5));
    afficherQuellePosition(5)=[];
    afficherQuellePosition(3)=[];
    afficherQuellePosition(1)=[];
    ydir=[0 1 0];
    for it=1:size(positions, 1)
        if(isempty(find(afficherQuellePosition==it, 1)))
            forces(it, :)= drive(nule, bsxfun(@plus, tilt_magnet(aimant, -positions_x(it)), positions(it, :)), ...
            -positions_x(it), position_bobine, orientation_bobine, dipolar_momentum, courant);
            tensions(it, :) = dot(cross(-longueur*[sin(positions_x(it)), 0, cos(positions_x(it))], ydir), forces(it, :));
            size(ydir);
            size(forces(it, :));
            size(-longueur*[sin(positions_x(it)), 0, cos(positions_x(it))]);
        else
            forces(it, :)= drive([0 0 1 1 1], bsxfun(@plus, tilt_magnet(aimant, -positions_x(it)), positions(it, :)), ...
            -positions_x(it), position_bobine, orientation_bobine, dipolar_momentum, courant);
            tensions(it, :) = dot(cross(-longueur*[sin(positions_x(it)), 0, cos(positions_x(it))], ydir), forces(it, :));
        end
    end
    tensions=tensions*omega;
    tensions=tensions/courant;
    % reste a calculer la norme de la force utile
    forces_x=forces(:, 1);
    
    figure;
    plot(positions_x, forces_x, 'r'); % Dans notre cas, la composante
    hold on;
    plot(positions_x, forces(:,2), 'g');
    hold on;
    plot(positions_x, forces(:,3), 'b');
    title('Force exerc�e sur l''aimant, ');
    
    figure;
    plot(positions_x, tensions(:, 1), 'r'); % Dans notre cas, la composante
    hold on;
    plot(positions_x, tensions(:,2), 'g');
    hold on;
    plot(positions_x, tensions(:,3), 'b');
    title('Tension induite');
    
    figure;
    plot(positions_x, positions(:,1), 'r'); hold on;
    plot(positions_x, positions(:,2), 'g'); hold on;
    plot(positions_x, positions(:,3), 'b'); hold on;
    plot(positions_x, positions_x, 'g');
    title('positions');
    
    figure;
    plot3(positions(:,1), positions(:,2), positions(:,3));
    title('Trajectoire parcourue par l''aimant');
    
    % en y est nulle et z n'influence pas.
end


%% Fonctions de discr�tisation
% (Elles ont �t� �dit�es afin de mieux correspondre � notre simulation

    function [magnet] = discretize_magnet(params, sampling)
    % return the position of each subdivision of the magnet relative to the
    % center of that magnet.
    % 
    % params : geometrical parameters of the magnet: [radius height]
    %          or [length, width, height] depending on the shape
    % sampling : sampling of the magnet in the x, y and z directions.

        radius = params(1);
        height = params(2);
        % Discretization of the space
        x = 1.1*linspace(-radius, radius, sampling(1));
        y = 1.1*linspace(-radius, radius, sampling(2));
        z = linspace(-height/2, height/2, sampling(3)+1);
            z = z+0.5*(z(2)-z(1)); % decalage vers le haut d'un demi intervalle
            z(end) = [];
        % use function repmat: repeats a matrix.
        x = repmat(x, 1, sampling(2)*sampling(3));
            x = x(:); % remet en 1 colonne
        y = repmat(y, sampling(1), sampling(3));
            y = y(:);
        z = repmat(z, sampling(1)*sampling(2), 1);
            z = z(:);

        % eliminate the positions outside the magnet
        ind = (x.^2+y.^2)> radius^2;
        x(ind) = [];
        y(ind) = [];
        z(ind) = [];
        %Les elements sont ranges par z, puis y, puis x croissants.
        %Vu dans un repere classique, il y a des couches selon z,
        % puis selon x. 
        magnet = [x,y,z];
    end
    
    function [tiltedMagnet] = tilt_magnet(magnet, angle)
        tiltedMagnet=magnet*[ cos(angle), 0, -sin(angle) ; 0, 1, 0 ; sin(angle), 0, cos(angle)];
    end

    function [loop_position, loop_orientation] = discretize_coil(...
        inner_radius, outer_radius, height, sampling, nbOnHeight, nbOnSurface)
    % retourne la position et l'orientation de chaque subdivision de la
    % bobine, par rapport au centre, en tenant compte de notre techno de
    % bobine (rayon des boucles variable)
    
    % inner_radius : rayon interne de la bobine
    % outer_radius : rayon externe
    % height : hauteur de la bobine
    % sampling : �chantillonage sur une seule boucle
    % nbOnLength : nombre de couche sur la hauteur
    % nbOnSurface : nombre de boucles par couche, entre le rayon interne et
    % le rayon externe
    
    % loop_position : Nx3 contenant les positions de chaque morceau
    % loop_orientation : Nx3 contenant les orienations de chaque morceau
    
        % matrices preliminaires
        thetas=linspace(0, 2*pi, sampling+1); % les angles
            thetas(end)=[];
        heights=linspace(-height/2, height/2, nbOnHeight); % les hauteurs
        radiuses=linspace(inner_radius, outer_radius, nbOnSurface); % les rayons
        
        % calcul des coordonnees
        x=radiuses'*cos(thetas); % ligne: rayon fixe, colonne: angle fixe
            x=repmat(x, 1, size(heights, 2));
            x=x(:); % pour le mettre en colonne
        y=radiuses'*sin(thetas);
            y=repmat(y, 1, size(heights, 2));
            y=y(:);
        z=repmat(heights, sampling*nbOnSurface, 1);
            z=z(:);
        
        % on rassemble tout
        loop_position=[x y z];
        % calcul des dl
        zdir=zeros(size(loop_position));
        zdir(:,3)=1;
        loop_orientation=2*pi/sampling * cross(loop_position', zdir')';
    end



