function [force_total] = drive(affichage, aimant, tilt, ...
    position_bobine, orientation_bobine, dipolar_momentum, courant)
% Le but de cette fonction est de donner la force s'exercant sur la bobine
% pour une position donnee de l'aimant.

% position_aimant : vecteur a trois entrees donnant la position de l'aimant
%                   par rapport au centre de la bobine
% affichage : Tableau a quatre elemnts : faut-il afficher les graphe?
%             1=oui, 0=non. Graphes : discret de l'aimant, bobine, le tout
%             et le champ

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Affichage de l'arrangement etudie

    if affichage(1)
        % affichage des morceaux d'aimant
        figure
        plot3(aimant(:,1),aimant(:,2),aimant(:,3),'.b')
        axis equal;
        title('Discretisation de l''aimant')
        xlabel('x'), ylabel('y'), zlabel('z');
    end
    %aimant
    %tilt
    
    % simple loop, ech=8, distance=3mm  : 0, 0, -0.010514
    % simple loop, ech=8, distance=10mm : 0, 0, -0.001892
    % grosse loop, ech=8, distance=3mm  : 0, 0, -0.489151
    % grosse loop, ech=8, distance=10mm : 0, 0, -0.045046
    % en moyenne, 45 fois plus, rien que gr�ce � la bobine
    if affichage(2)
        % affichage des morceaux de bobine
        figure;
        pts1 = position_bobine-orientation_bobine/2;
        pts2 = position_bobine+orientation_bobine/2;
        plot3( [pts1(:,1), pts2(:,1)]', [pts1(:,2), pts2(:,2)]', ...
            [pts1(:,3), pts2(:,3)]', '.-r');
        axis equal;
        title('Forme de la bobine discretisee');
        xlabel('x'), ylabel('y'), zlabel('z');
    end

    if affichage(3)
        % affichage du tout
        figure;
            plot3(aimant(:,1), aimant(:,2), aimant(:,3),'.b')
            axis equal;
            hold on
            pts1 = position_bobine-orientation_bobine/2;
            pts2 = position_bobine+orientation_bobine/2;
            plot3( [pts1(:,1), pts2(:,1)]', [pts1(:,2), pts2(:,2)]',...
                [pts1(:,3), pts2(:,3)]', '.-r');
            axis equal;
            title('Structure etudiee');
            xlabel('x'), ylabel('y'), zlabel('z');
    end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% computation of the magnetic field on each part of the loop

    B_elem = dipolar_field(aimant, position_bobine, dipolar_momentum, tilt);
    
    force_bob = B_to_F(B_elem, orientation_bobine, courant);
    
    force_x=sum(force_bob(:,1));
    force_y=sum(force_bob(:,2));
    force_z=sum(force_bob(:,3));
    force_total=[force_x, force_y, force_z];
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% affichage des resultats
    if affichage(5)
        disp('Force exercee sur la bobine (avec sommation) : ');
        disp(force_total);
    end
    
end

%%%%%%%%%%%%%%%%%%%%%%%%
%% Fonctions a completer
%%%%%%%%%%%%%%%%%%%%%%%%

    function B = dipolar_field(r_source, r_rec, dipolar_momentum, angle)
        % Renvoie le champ magnetique induit aux points r_rec par des dipoles
        % situes au point r_source. "dipolar_momentum" est le moment dipolaire
        % total de l'aimant.

        % r_source : Nx3. Chaque ligne represente les coordonnees d'une
        %            fraction de l'aimant.
        % r_rec    : vecteur Mx3 : position de chaque troncon de boucle
        % dipolar_momentum : Moment magnetique total de l'aimant.

        % B = [Bx, By, Bz] : Champ magnetique total au niveau du troncon
        %                    de la bobine.

        % A EFFECTUER : RENDRE TOUT VECTORIEL POUR ENLEVER LE FOR ET
        % L'ITERATEUR
            % moment magnetique partiel de chaque dipole
            m0 = dipolar_momentum/size(r_source,1);

            % distance de chaque r_rec a chaque dipole
            % delta_x : N lignes, M colonnes (1 ligne par r_source, 1 col par
            % r_rec)
            % avant : N lignes, 1 colonne
            [r_recx, r_sourcex]=meshgrid(r_rec(:,1),r_source(:,1));
            % meshgrid renvoie deux tableaux, de taille NxM, pour faire la
            % ligne suivante
            delta_x0 = r_recx - r_sourcex;

            [r_recy, r_sourcey]=meshgrid(r_rec(:,2),r_source(:,2));
            delta_y = r_recy - r_sourcey;
            [r_recz, r_sourcez]=meshgrid(r_rec(:,3),r_source(:,3));
            delta_z0 = r_recz - r_sourcez;
            r = sqrt( delta_x0.^2 + delta_y.^2 + delta_z0.^2);
            
            % tiltage
            
            delta_x = delta_x0 * cos(angle) - delta_z0 * sin(angle);
            % delta_y reste inchange
            delta_z = delta_x0 * sin(angle) + delta_z0 * cos(angle);

            % terme commun aux trois coordonnees : muem
            % moment partiel * (permeabilite vide/4*pi) / 1/r^5
            muem = (m0 * (1e-7))./(r.^(5));

            % composantes de chacun des bouts d'aimant
            dbx= 3.*delta_x.*delta_z.*muem;       % muem*3xz
            dby= 3.*delta_y.*delta_z.*muem;       % muem*3yz
            dbz = (3.*(delta_z.^2) - r.^2).*muem; % muem*(3z^2-r^2)

            % resultante des champs de chacun des dipoles
            Bx=(sum(dbx))';
            By=(sum(dby))';
            Bz=(sum(dbz))';
            Btemp=[Bx By Bz]; % champ, dans la base aimant
            B=Btemp*[ cos(angle), 0, -sin(angle) ; 0, 1, 0 ; sin(angle), 0, cos(angle)];
    end
    
    function force_bob = B_to_F(B_elem, orientation_bobine, i)
        % Calcule la force s'exercant sur chaque element de la bobine.
        
        % B_elem : Nx3; la ligne "i" contient le champ magnetique
        %          au niveau de l'element "i" de la bobine
        % orientation_bobine : Nx3; la ligne "i" contient le vecteur
        %                      "orientation" de l'element "i" de la bobine
        % i : courant dans la bobine
        
        % force_bob: Nx3; la ligne "i" contient la force s'exercant
        %              sur l'element "i" de la bobine.
        
        
        % calcul (enfin !) de la Force, par maitre Yoda
        force_bob = i* cross(orientation_bobine', B_elem')';
        
    end

    

