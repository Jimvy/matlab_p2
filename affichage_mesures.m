%% Partie Mesures
load('data.mat');
freq=0.0001;
numech=4000;
time = (0:freq:numech*freq);
time=time-0.0001*2147;
time(end)=[];
[Mt, indexmaxt]=max(Mesures);
[mt, indexmint]=min(Mesures);
delta_t=0.0001*(indexmaxt-indexmint);
delta_V=Mt-mt;

%% Partie vieilles forces
[positions_x, positions_x2, prevForces, forces]=main();
[Md2, indexmaxd2]=max(prevForces);
[md2, indexmind2]=min(prevForces);
[Md, indexmaxd]=max(forces);
[md, indexmind]=min(forces);
delta_x=(positions_x(2)-positions_x(1))*(indexmaxd-indexmind);
delta_x2 = (positions_x2(2)-positions_x2(1))*(indexmaxd2-indexmind2);
delta_F=Md-md;
delta_F2=Md2-md2;

%% Proportionnalité et affichage
vitesse=delta_x / delta_t;
coeff=delta_F/delta_V;

F2coeff=delta_F/delta_F2;
x2coeff=delta_x/delta_x2;

figure;
plot(time*vitesse, Mesures*coeff, '-r');
hold on;
plot(positions_x2*x2coeff, prevForces*F2coeff, '-.g');
hold on;
plot(positions_x, forces, '--b');
legend('Mesure expérimentale','Simulation numérique S13','Simulation numérique améliorée');
